use ejercicios;

-- Eliminar si existe la tabla agenda.
drop table if exists agenda;

-- Crear la tabla agenda: id(que se llene con ceros, auto incremental y llave primaria), apellido, nombre, correo, fecha de ingreso, teléfono.

create table agenda (
    id int(3) zerofill auto_increment,
    apellido varchar(45),
    nombre varchar(45),
    correo varchar(45),
    fecha_ingreso datetime,
    telefono varchar(20),
    primary key(id)
);

-- visualizar la estructura de la tabla que se ha creado.
describe agenda;

/*
4-Ingrese los siguientes registros: (Lopez,Alberto,Colon 123,4234567), (Cifuentes,Juan,Avellaneda 135,4458787), (Lopez,Mariana Urquiza 333,4545454), (Mores,Jose,Urquiza 333,4545454), (Peralta,Susana,Gral. Paz 1234,4123456).
*/

insert into agenda values 
(null,'Lopez','Alberto','Colon@fake.dev', now(),4234567),
(null,'Cifuentes','Juan','Avellaneda@fake.dev', now(),4458787),
(null,'Lopez','Mariana','Urquiza@fake.dev', now(),4234567),
(null,'Mores','Jose','Urquiza@fake.dev', now(),4234567);

-- Modificar el registro cuyo nombre sea "Juan" por "Juan José"

select * from agenda;
update agenda set nombre = 'Juan José' where nombre = 'Juan';
select * from agenda;
-- Actualizar los registros cuyo número telefónico sea igual a '4545454' por '4445566'

update agenda set telefono = '4545454' where telefono = 'Juan';
select * from agenda;