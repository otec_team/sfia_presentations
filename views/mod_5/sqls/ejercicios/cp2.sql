-- 1- Recuperar los registros cuyo actor sea "Tom Cruise" o "Richard Gere".

select  * 
from    pelicula 
where   actor = 'Tom Cruise' 
    or  actor = 'Richard Gere';

-- 2- ¿Cuantos registros cuyo actor sea "Tom Cruise" y "Richard Gere", existen?.
select  count(*)
from    pelicula
where   actor = 'Tom Cruise' 
    and actor = 'Richard Gere';


-- 3- Cambie la duración a 200, de las películas cuyo actor sea "Daniel R." y cuya duración sea 180.
update  pelicula
set     duracion = 200
where   duracion = 180;

-- 4- Borre todas las películas donde el actor NO sea "Tom Cruise" y cuya duración sea mayor o igual a 100
delete
from    pelicula
where   actor <> 'Tom Cruise'
    and duracion >= 100;

-- 5- ingrese 3 de sus películas favoritas.