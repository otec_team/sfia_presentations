-- borramos la tabla si esta existe:
drop table if exists tejemplo;

-- creamos la tabla con 10 registros:
create table tejemplo (
    id int(10) unsigned auto_increment,
    campo_1 varchar(45),
    campo_2 int(10),
    campo_3 int(10),
    primary key(id)
);

-- insertamos los valores de prueba:
insert into tejemplo (campo_1, campo_2, campo_3) values
('valor1_1', 0, 1),
('valor2_1', 2, 21),
('valor3_1', 4, 12),
('valor4_1', 5, 13),
('valor5_1', 7, 14),
('valor6_1', 8, 17),
('valor7_1', 2, 19),
('valor8_1', 9, 14),
('valor9_1', 7, 16);

-- mostramos los datoss ingresados
select * from tejemplo;

select 'Datos cargados, buena suerte 😎';

-- seleccionamos con un where usando and
select  * 
from    tejemplo
where   campo_2 >= 2
    and campo_3 <= 16;

-- seleccionamos usando el or
select  *
from    tejemplo
where   campo_1 = 'valor1_1'
    or  campo_2 = 7
    or  campo_3 = 14;

-- seleccionamos usando el xor (true, false)
select  *
from    tejemplo
where   campo_1 = 'valor1_1'
    xor campo_2 = 70;

-- seleccionamos usando el xor (true, true)
select  *
from    tejemplo
where   campo_1 = 'valor1_1'
    xor campo_1 = 'valor2_1';

-- seleccionamos usando el xor (false, true)
select  *
from    tejemplo
where   campo_1 = 'valor1_11'
    xor campo_1 = 'valor2_1';

-- seleccionamos usando el xor (false, false, true)
select  *
from    tejemplo
where   campo_1 = 'valor1_11'
    xor campo_1 = 'valor2_11'
    xor campo_1 = 'valor3_1';

-- seleccionamos usando el xor (false, true, false)
select  *
from    tejemplo
where   campo_1 = 'valor1_11'
    xor campo_1 = 'valor2_1'
    xor campo_1 = 'valor3_11';

-- seleccionamos usando el xor (true, true, false)
select  *
from    tejemplo
where   campo_1 = 'valor1_1'
    xor campo_1 = 'valor2_1'
    xor campo_1 = 'valor3_11';

-- seleccionamos usando el xor (true, false, true, false)
select  *
from    tejemplo
where   campo_1 = 'valor1_1'
    xor campo_1 = 'valor2_11'
    xor campo_1 = 'valor3_1'
    xor campo_1 = 'valor3_11';

-- :::::::::::::::::::::::::::::not:::::::::::::::::::::::::::
select  * 
from    tejemplo
where   not campo_1 =  'valor1_1';







