DROP TABLE IF EXISTS `ciudad`;

CREATE TABLE `ciudad` (
    `ID` INT(3) ZEROFILL AUTO_INCREMENT,
    `nombre` VARCHAR(45) NOT NULL DEFAULT '',
    `sigla` VARCHAR(45) NOT NULL DEFAULT '',
    `estado` VARCHAR(45) NOT NULL DEFAULT '',
    `poblacion` INT(11) NOT NULL DEFAULT '0',
    PRIMARY KEY (`ID`)
);

-- insertamos los datos:
INSERT INTO `ciudad` 
VALUES      (null,'Kabul','AFG','Kabol',1780000),
            (null,'Qandahar','AFG','Qandahar',237500),
            (null,'Herat','AFG','Herat',186800),
            (null,'Mazar-e-Sharif','AFG','Balkh',127800),
            (null,'Amsterdam','NLD','Noord-Holland',731200),
            (null,'Rotterdam','NLD','Zuid-Holland',593321),
            (null,'Haag','NLD','Zuid-Holland',440900),
            (null,'Utrecht','NLD','Utrecht',234323),
            (null,'Eindhoven','NLD','Noord-Brabant',201843),
            (null,'Tilburg','NLD','Noord-Brabant',193238),
            (null,'Groningen','NLD','Groningen',172701),
            (null,'Breda','NLD','Noord-Brabant',160398),
            (null,'Apeldoorn','NLD','Gelderland',153491),
            (null,'Nijmegen','NLD','Gelderland',152463),
            (null,'Enschede','NLD','Overijssel',149544),
            (null,'Haarlem','NLD','Noord-Holland',148772),
            (null,'Almere','NLD','Flevoland',142465),
            (null,'Arnhem','NLD','Gelderland',138020),
            (null,'Zaanstad','NLD','Noord-Holland',135621),
            (null,'s-Hertogenbosch','NLD','Noord-Brabant',129170),
            (null,'Amersfoort','NLD','Utrecht',126270),
            (null,'Maastricht','NLD','Limburg',122087),
            (null,'Dordrecht','NLD','Zuid-Holland',119811);