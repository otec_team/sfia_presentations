drop table if exists pelicula;

create table pelicula ( 
    id       int (10) zerofill auto_increment, 
    titulo   varchar(45), 
    actor    varchar(45), 
    duracion int(3) unsigned, 
    primary key (id) 
);

insert into pelicula (titulo,actor,duracion) 
values      ('Mision imposible','Tom Cruise',120),
            ('Harry Potter y la piedra filosofal','Daniel R.',180),
            ('Harry Potter y la camara secreta','Daniel R.',190),
            ('Mision imposible 2','Tom Cruise',120),
            ('Mujer bonita','Richard Gere',120),
            ('Tootsie','D. Hoffman',90),
            ('Un oso rojo','Julio Chavez',100),
            ('Elsa y Fred','China Zorrilla',110);






